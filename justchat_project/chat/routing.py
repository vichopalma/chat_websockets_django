from django.conf.urls import re_path

from .consumers import ChatConsumer


#enrutador webSocket, aqui anadimos la ruta al cual se conectará nuestro websocket
#y a su vez, le entregamos el respectivo consumidor
websocket_urlpatterns = [
    re_path(r'^ws/chat/(?P<room_name>[^/]+)/$', ChatConsumer),
]