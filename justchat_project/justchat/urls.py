from django.contrib import admin
from django.urls import path, include

#urls de nuestro proyecto
urlpatterns = [
    path('admin/', admin.site.urls),
    path('chat/', include('chat.urls', namespace='chat')),
]
